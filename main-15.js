var bus = new Vue();

Vue.component('message', {

    template: '<div><input v-model="message" @keyup.enter="storeMessage"></div>',

    data: function () {
        return {
            message: ''
        };
    },

    methods: {
        storeMessage: function () {

            bus.$emit('new-message', this.message);

            this.message = '';
        }
    }
});


new Vue({
    el: '#app',

    methods: {
        handleMessage: function (message) {
            console.log('Parent is handling ' + message);
        }
    },

    created: function () {
        bus.$on('new-message', this.handleMessage);
    },

    destroyed: function () {
        bus.$off('new-message', this.handleMessage);
    }

})