Vue.component('tasks', {
    props: ['list'],

    computed: {
      remaining: function () {
        return this.list.filter(this.inProgress).length;
      }
    },

    methods: {
      isCompleted: function(task){
        return task.completed;
      },

      inProgress: function(task){
        return ! this.isCompleted(task);
      },

      deleteTask: function(task){
        this.list.splice(this.list.indexOf(task), 1);
      },

      clearCompleted: function(){
        this.list = this.list.filter(this.inProgress);
      }
    },

    template: '#tasks-template',
});

new Vue({
    el: '#app',

    data: {
        tasks: [{
            body: 'Go to the store',
            completed: false
        }, {
            body: 'Go to the bank',
            completed: false
        }, {
            body: 'Go to the doctor',
            completed: true
        }, ]
    }
})
