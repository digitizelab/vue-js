Vue.filter('role', function (value, role) {
    return value.filter(function (item) {
        return item.role == role;
    })
});


Vue.filter('jsonIt', function (value) {
    return JSON.stringify(value);
})

new Vue({
    el: '#app',

    data: {
        message: 'Hello World',

        people: [
            { name: 'Joe', role: 'admin' },
            { name: 'Susan', role: 'admin' },
            { name: 'Frank', role: 'student' },
            { name: 'Jeffrey', role: 'admin' },

        ]
    }
})